var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            _super.apply(this, arguments);
        }
        Boot.prototype.preload = function () {
        };
        Boot.prototype.create = function () {
            this.input.maxPointers = 1;
            this.stage.disableVisibilityChange = true;
            this.game.state.start("TheCity", true, false);
        };
        return Boot;
    })(Phaser.State);
    TheElevatorPitchModule.Boot = Boot;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
/// <reference path="Ts/Boot.ts" />
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    var TheElevatorPitch = (function (_super) {
        __extends(TheElevatorPitch, _super);
        function TheElevatorPitch() {
            _super.call(this, 800, 600, Phaser.AUTO, 'content', null);
            this.state.add('Boot', TheElevatorPitchModule.Boot, false);
            this.state.add('TheCity', TheElevatorPitchModule.TheCity, false);
            this.state.start("Boot", false, false);
        }
        return TheElevatorPitch;
    })(Phaser.Game);
    window.onload = function () {
        var game = new TheElevatorPitch();
    };
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var CloudServiceProvider = (function () {
        function CloudServiceProvider(game, utils, dayLight) {
            this.game = game;
            this.utils = utils;
            this.dayLight = dayLight;
            this.clouds = [];
        }
        CloudServiceProvider.prototype.Update = function (meteoContitions) {
            for (var i = 0; i < this.clouds.length; i++) {
                if (this.clouds[i] != null) {
                    this.game.world.bringToTop(this.clouds[i]);
                }
            }
            this.meteoContitions = meteoContitions;
            if (this.meteoContitions.spawnCloud) {
                var selector = "cloud" + this.game.rnd.integerInRange(1, 6);
                var cloud = this.game.cache.getImage(selector);
                var isRightToLeft = this.game.rnd.integerInRange(1, 2) === 2;
                var perc = this.game.rnd.integerInRange(20, 40);
                var nextW = (cloud.width * perc) / 100;
                var nextH = (cloud.height * perc) / 100;
                var prevX = 0 - nextW;
                var nextX = this.game.width;
                if (isRightToLeft) {
                    prevX = this.game.width + nextW;
                    nextX = 0 - nextW;
                }
                var sprite = this.game.add.sprite(prevX, this.game.rnd.integerInRange(-10, (this.game.height / 4) - 30), selector);
                this.clouds.push(sprite);
                sprite.width = nextW;
                sprite.height = nextH;
                sprite.alpha = this.game.rnd.integerInRange(80, 90);
                sprite.anchor.set(isRightToLeft ? 1 : 0, sprite.anchor.y);
                var startTint = this.utils.RGBtoHEX(255, 255, 255);
                sprite.tint = startTint;
                var endTint = this.utils.RGBtoHEX(0, 0, 0);
                var speed = this.game.rnd.integerInRange(this.dayLight * 4, this.dayLight * 4);
                var cloudSpeedEasing = Phaser.Easing.Elastic.InOut;
                var cloudTintEasing = Phaser.Easing.Default;
                if (this.meteoContitions.cloudSpeed > 0) {
                    speed = this.meteoContitions.cloudSpeed;
                    cloudSpeedEasing = Phaser.Easing.Linear.None;
                    cloudTintEasing = Phaser.Easing.Exponential.Out;
                }
                if (this.meteoContitions.tint) {
                    this.utils.tweenTint(sprite, startTint, endTint, speed, cloudTintEasing);
                }
                this.game.add.tween(sprite).to({ x: nextX }, speed, cloudSpeedEasing, true)
                    .onComplete.add(function (s, t) {
                    s.destroy();
                });
            }
        };
        return CloudServiceProvider;
    })();
    TheElevatorPitchModule.CloudServiceProvider = CloudServiceProvider;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var DayNightServiceProviderOption = (function () {
        function DayNightServiceProviderOption(backgroundSprites, sunSprite, moonSprite, starsSprite, meteoServiceProvider) {
            this.BackgroundSprites = backgroundSprites;
            this.SunSprite = sunSprite;
            this.MoonSprite = moonSprite;
            this.StarsSprite = starsSprite;
            this.MeteoServiceProvider = meteoServiceProvider;
        }
        return DayNightServiceProviderOption;
    })();
    TheElevatorPitchModule.DayNightServiceProviderOption = DayNightServiceProviderOption;
    var DayNightServiceProvider = (function () {
        function DayNightServiceProvider(game, dayLenght, utils, options, trafficServiceProvider) {
            this.Hour = 0;
            this.CurrentDate = new Date();
            this.game = game;
            this.utils = utils;
            this.dayLenght = dayLenght;
            this.shading = null;
            this.sunSprite = null;
            this.moonSprite = null;
            this.starsSprite = options.StarsSprite;
            this.TimeLapse = this.dayLenght / 12;
            this.motionSpeed = dayLenght + (dayLenght / 32);
            this.trafficServiceProvider = trafficServiceProvider;
            this.meteoServiceProvider = options.MeteoServiceProvider;
            this.initClockText();
            this.initShading(options.BackgroundSprites);
            this.initSun(options.SunSprite);
            this.initMoon(options.MoonSprite);
        }
        DayNightServiceProvider.prototype.initClockText = function () {
            this.clockText = this.CurrentDate.toDateString() + " " + (this.Hour < 10 ? "0" + this.Hour : this.Hour + "") + ":00";
            this.ClockTime = this.game.add.text(10, 10, this.clockText, {});
            this.ClockTime.fontSize = 12;
            this.ClockTime.addColor("white", 0);
        };
        DayNightServiceProvider.prototype.initSun = function (sprite) {
            this.sunSprite = sprite;
            this.sunRise(sprite);
        };
        DayNightServiceProvider.prototype.initMoon = function (moon) {
            this.moonSprite = moon;
            this.moonSet(moon);
        };
        DayNightServiceProvider.prototype.sunRise = function (sprite) {
            var _this = this;
            this.CurrentDate.setDate(this.CurrentDate.getDate() + 1);
            sprite.position.x = this.game.width - sprite.width - (sprite.width / 2);
            this.sunTween = this.game.add.tween(sprite).to({ y: 0 - (sprite.height * 2) }, this.motionSpeed, Phaser.Easing.Circular.Out, true);
            this.utils.tweenTint(sprite, 0xff6600, 0xffff00, this.motionSpeed, Phaser.Easing.Default);
            this.sunTween.onComplete.add(this.sunSet, this);
            if (this.TimerEvent == null) {
                this.TimerEvent = this.game.time.events.loop(this.TimeLapse, function () {
                    if (_this.Hour === 24) {
                        _this.Hour = 0;
                    }
                    else {
                        _this.Hour++;
                    }
                    _this.clockText = (_this.Hour < 10 ? "0" + _this.Hour : _this.Hour + "") + ":00";
                    _this.ClockTime.text = _this.CurrentDate.toDateString() + " " + _this.clockText;
                });
            }
            if (this.shading) {
                this.shading.forEach(function (sprite) {
                    _this.utils.tweenTint(sprite.sprite, sprite.from, sprite.to, _this.motionSpeed, Phaser.Easing.Linear.None);
                });
            }
        };
        DayNightServiceProvider.prototype.sunSet = function (sprite) {
            var _this = this;
            sprite.position.x = sprite.width + (sprite.width / 2);
            this.sunTween = this.game.add.tween(sprite)
                .to({ y: this.game.world.height + (sprite.height * 2) }, this.motionSpeed, Phaser.Easing.Circular.In, true);
            this.utils.tweenTint(sprite, 0xffff00, 0xff6600, this.dayLenght, Phaser.Easing.Circular.In);
            this.sunTween.onComplete.add(this.sunRise, this);
            if (this.shading) {
                this.shading.forEach(function (sprite) {
                    _this.utils.tweenTint(sprite.sprite, sprite.to, sprite.from, _this.motionSpeed, Phaser.Easing.Circular.In);
                });
            }
        };
        DayNightServiceProvider.prototype.moonRise = function (sprite) {
            sprite.position.x = this.game.width - sprite.width - (sprite.width / 2);
            this.moonTween = this.game.add.tween(sprite).to({ y: 0 - (sprite.height * 2) }, this.motionSpeed, Phaser.Easing.Circular.Out, true);
            this.moonTween.onComplete.add(this.moonSet, this);
            // unusable
            // var spriteB = this.game.add.sprite(-100, -100, this.starsSprite.generateTexture());
            // spriteB.x = 50;
            // spriteB.y = 50;
            // let ow = spriteB.width;
            // let oh = spriteB.height;
            // spriteB.width = 50;
            // spriteB.height = (oh * 50) / ow;
            // spriteB.alpha = 0;
            // let tween = this.game.add.tween(spriteB).to({ alpha: 50 }, 1000, Phaser.Easing.Elastic.Out, true,7000)
            //  .onComplete.add(function (sprite: Phaser.Sprite, tween: Phaser.Tween, game: Phaser.Game) {
            //    game.add.tween(sprite).to({ alpha: 0 }, 4000, Phaser.Easing.Elastic.In, true)
            //      .onComplete.add(function (s: Phaser.Sprite) {s.destroy() });
            //  }, this, 0, this.game);
        };
        DayNightServiceProvider.prototype.moonSet = function (moon) {
            moon.position.x = moon.width + (moon.width / 2);
            this.moonTween = this.game.add.tween(moon)
                .to({ y: this.game.world.height + (moon.height * 2) }, this.motionSpeed, Phaser.Easing.Circular.In, true);
            this.moonTween.onComplete.add(this.moonRise, this);
        };
        DayNightServiceProvider.prototype.initShading = function (sprites) {
            this.shading = sprites;
        };
        DayNightServiceProvider.prototype.Update = function () {
            this.trafficServiceProvider.Update(this.CurrentDate, this.Hour);
            this.meteoServiceProvider.Update(this.CurrentDate, this.Hour);
        };
        return DayNightServiceProvider;
    })();
    TheElevatorPitchModule.DayNightServiceProvider = DayNightServiceProvider;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var MeteoResult = (function () {
        function MeteoResult() {
            this.cloudSpeed = 0;
            this.shouldUpdate = true;
        }
        return MeteoResult;
    })();
    TheElevatorPitchModule.MeteoResult = MeteoResult;
    var SeasonInfo = (function () {
        function SeasonInfo(name, startMonth) {
            this.Name = name;
            this.StartMonth = startMonth;
        }
        return SeasonInfo;
    })();
    TheElevatorPitchModule.SeasonInfo = SeasonInfo;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    (function (eMeteoConditions) {
        eMeteoConditions[eMeteoConditions["Sunny"] = 0] = "Sunny";
        eMeteoConditions[eMeteoConditions["ModeratelyCloudly"] = 1] = "ModeratelyCloudly";
        eMeteoConditions[eMeteoConditions["Cloudly"] = 2] = "Cloudly";
        eMeteoConditions[eMeteoConditions["Shitty"] = 3] = "Shitty";
        eMeteoConditions[eMeteoConditions["Snow"] = 4] = "Snow"; // tbd
    })(TheElevatorPitchModule.eMeteoConditions || (TheElevatorPitchModule.eMeteoConditions = {}));
    var eMeteoConditions = TheElevatorPitchModule.eMeteoConditions;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var MeteoPredictor = (function () {
        function MeteoPredictor(game) {
            this.dateDay = undefined;
            this.duration = 0;
            this.day = 0;
            this.game = game;
            this.seasons = [];
            this.seasons.push(new TheElevatorPitchModule.SeasonInfo("Spring", 3));
            this.seasons.push(new TheElevatorPitchModule.SeasonInfo("Summer", 6));
            this.seasons.push(new TheElevatorPitchModule.SeasonInfo("Autumn", 9));
            this.seasons.push(new TheElevatorPitchModule.SeasonInfo("Winter", 12));
        }
        MeteoPredictor.prototype.Predict = function (date, hour) {
            var result = new TheElevatorPitchModule.MeteoResult();
            var force = false;
            var currentDay = date.getDate();
            if (this.dateDay === undefined) {
                this.dateDay = currentDay;
                this.setCondition();
                result.shouldUpdate = true;
                force = true;
            }
            if (this.dateDay !== currentDay) {
                this.day++;
                this.dateDay = currentDay;
                if (this.duration === this.day) {
                    this.day = 0;
                    this.setCondition();
                    result.shouldUpdate = true;
                }
            }
            else {
                if (!force)
                    result.shouldUpdate = false;
            }
            result.conditions = this.condition;
            this.setClouds();
            result.cloudSpeed = this.cloudSpeed;
            result.spawnCloud = this.spawnCloud;
            result.tint = this.tint;
            return result;
        };
        MeteoPredictor.prototype.setCondition = function () {
            //this.duration = 1;
            //this.condition = eMeteoConditions.Shitty;
            this.duration = this.game.rnd.integerInRange(1, this.game.rnd.integerInRange(3, 10));
            var prob = this.game.rnd.integerInRange(1, 100);
            if (prob <= 10) {
                this.condition = TheElevatorPitchModule.eMeteoConditions.Sunny;
            }
            else if (prob > 10 && prob <= 40) {
                this.condition = TheElevatorPitchModule.eMeteoConditions.ModeratelyCloudly;
            }
            else if (prob > 40 && prob <= 60) {
                this.condition = TheElevatorPitchModule.eMeteoConditions.Cloudly;
            }
            else if (prob > 60) {
                this.condition = TheElevatorPitchModule.eMeteoConditions.Shitty;
            }
        };
        MeteoPredictor.prototype.setClouds = function () {
            this.tint = false;
            if (this.condition == TheElevatorPitchModule.eMeteoConditions.Sunny) {
                this.spawnCloud = this.game.rnd.integerInRange(1, 100000) < 100;
            }
            else if (this.condition == TheElevatorPitchModule.eMeteoConditions.ModeratelyCloudly) {
                this.spawnCloud = this.game.rnd.integerInRange(1, 100000) < 1000;
            }
            else if (this.condition == TheElevatorPitchModule.eMeteoConditions.Cloudly) {
                this.spawnCloud = this.game.rnd.integerInRange(1, 100000) < 5000;
            }
            else if (this.condition == TheElevatorPitchModule.eMeteoConditions.Shitty) {
                this.spawnCloud = this.game.rnd.integerInRange(1, 100000) < 70000;
                this.cloudSpeed = 10000;
                this.tint = true;
            }
            else if (this.condition == TheElevatorPitchModule.eMeteoConditions.Snow) {
                this.spawnCloud = this.game.rnd.integerInRange(1, 100000) < 10;
            }
        };
        return MeteoPredictor;
    })();
    TheElevatorPitchModule.MeteoPredictor = MeteoPredictor;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var MeteoServiceProvider = (function () {
        function MeteoServiceProvider(game, utils, cloudServiceProvider, rainServiceProvider, dayLight) {
            this.dayLight = dayLight;
            this.game = game;
            this.utils = utils;
            this.CloudServiceProvider = cloudServiceProvider;
            this.RainServiceProvider = rainServiceProvider;
            this.meteoPredictor = new TheElevatorPitchModule.MeteoPredictor(game);
            this.initMeteoText();
        }
        MeteoServiceProvider.prototype.initMeteoText = function () {
            this.MeteoText = this.game.add.text(10, 30, "Meteo: /", {});
            ;
            this.MeteoText.fontSize = 12;
            this.MeteoText.addColor("white", 0);
        };
        MeteoServiceProvider.prototype.Update = function (date, hour) {
            var result = this.meteoPredictor.Predict(date, hour);
            var pip = ["Sunny", "ModeratelyCloudly", "Cloudly", "Shitty", "Snow"];
            this.MeteoText.text = "Meteo: " + pip[result.conditions];
            this.CloudServiceProvider.Update(result);
            if (result.shouldUpdate) {
                this.RainServiceProvider.Update(result);
            }
        };
        MeteoServiceProvider.prototype.putOnTop = function () {
            this.RainServiceProvider.putOnTop();
        };
        return MeteoServiceProvider;
    })();
    TheElevatorPitchModule.MeteoServiceProvider = MeteoServiceProvider;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var RainServiceProvider = (function () {
        function RainServiceProvider(game, utils, dayLight) {
            this.emitter = null;
            this.bitmapData = null;
            this.game = game;
            this.utils = utils;
            this.dayLight = dayLight;
        }
        RainServiceProvider.prototype.Update = function (meteoContitions) {
            if (meteoContitions.conditions === TheElevatorPitchModule.eMeteoConditions.Shitty) {
                this.Start();
            }
            else {
                this.Stop();
            }
        };
        RainServiceProvider.prototype.putOnTop = function () {
            if (this.emitter != null) {
                this.game.world.bringToTop(this.emitter);
            }
        };
        RainServiceProvider.prototype.Start = function () {
            if (this.emitter == null) {
                this.emitter = this.game.add.emitter(this.game.world.centerX, 200, 500);
                this.bitmapData = this.game.add.bitmapData(5, 500);
            }
            if (this.emitter.on == false) {
                this.bitmapData.ctx.rect(0, 0, 5, 1500); // todo: witdh should be variable
                this.bitmapData.ctx.fillStyle = "#9cc9de";
                this.bitmapData.ctx.fill();
                this.emitter.width = this.game.world.width;
                this.emitter.minParticleScale = 0.1;
                this.emitter.maxParticleScale = 0.1;
                this.emitter.setYSpeed(2500, 5000); // to do: variable
                this.emitter.setXSpeed(0, 0);
                this.emitter.minRotation = 0;
                this.emitter.maxRotation = 0;
            }
            this.emitter.makeParticles(this.bitmapData);
            this.game.time.events.add(5000, function () {
                this.emitter.start(false, this.dayLight, 1, 0);
            }, this);
        };
        RainServiceProvider.prototype.Stop = function () {
            if (this.emitter !== null && this.emitter.on == true) {
                this.emitter.on = false;
            }
        };
        return RainServiceProvider;
    })();
    TheElevatorPitchModule.RainServiceProvider = RainServiceProvider;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var TheCity = (function (_super) {
        __extends(TheCity, _super);
        function TheCity() {
            _super.apply(this, arguments);
        }
        TheCity.prototype.preload = function () {
            this.fetchImages();
        };
        TheCity.prototype.create = function () {
            var dayLight = 10000;
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.game.stage.backgroundColor = "#000";
            this.utils = new TheElevatorPitchModule.Utils(this.game);
            this.setBackgroundSprite();
            this.setDayAndNight();
            this.setSkyline();
            this.setLights();
            this.setSkyscraper();
            this.meteoServiceProvider = new TheElevatorPitchModule.MeteoServiceProvider(this.game, this.utils, new TheElevatorPitchModule.CloudServiceProvider(this.game, this.utils, dayLight), new TheElevatorPitchModule.RainServiceProvider(this.game, this.utils, dayLight), dayLight);
            this.trafficServiceProvider = new TheElevatorPitchModule.TrafficServiceProvider(this.game, this.utils, dayLight);
            this.dayNightServiceProvider = new TheElevatorPitchModule.DayNightServiceProvider(this.game, dayLight, this.utils, new TheElevatorPitchModule.DayNightServiceProviderOption(this.giveMeBackgroundSprites(), this.sunSprite, this.moonSprite, this.starsSprite, this.meteoServiceProvider), this.trafficServiceProvider);
        };
        TheCity.prototype.update = function () {
            this.putOnTop();
            this.dayNightServiceProvider.Update();
        };
        TheCity.prototype.fetchImages = function () {
            this.load.image("skyline", "Assets/images/skyline.png");
            this.load.image("skyscraper", "Assets/images/theskyscraper.png");
            this.load.image("moon", "Assets/images/stars_mandala_lg.png");
            this.load.image("stars", "Assets/images/stars.png");
            this.load.image("sun", "Assets/images/sun-2.png");
            this.load.image("lights", "Assets/images/lights.png");
            this.load.image("cloud1", "Assets/images/cloud1.png");
            this.load.image("cloud2", "Assets/images/cloud2.png");
            this.load.image("cloud3", "Assets/images/cloud3.png");
            this.load.image("cloud4", "Assets/images/cloud4.png");
            this.load.image("cloud5", "Assets/images/cloud5.png");
            this.load.image("cloud6", "Assets/images/cloud6.png");
            this.load.image("car1", "Assets/images/car-1.png");
            this.load.image("car2", "Assets/images/car-2.png");
            this.load.image("car3", "Assets/images/car-3.png");
            this.load.image("car4", "Assets/images/car-4.png");
            this.load.image("autobus1", "Assets/images/autobus500.png");
        };
        TheCity.prototype.setDayAndNight = function () {
            this.sunSprite = this.game.add.sprite(0, this.game.height, "sun");
            var w = 120;
            this.sunSprite.width = w;
            this.sunSprite.height = w;
            this.moonSprite = this.game.add.sprite(this.game.width, this.game.height, "moon");
            this.moonSprite.width = w;
            this.moonSprite.height = w;
            this.starsSprite = this.game.add.sprite(this.game.width, this.game.height, "stars");
            this.starsSprite.width = w;
            this.starsSprite.height = w;
        };
        TheCity.prototype.setBackgroundSprite = function () {
            this.backgroundSprite = this.game.add.sprite(0, 0, this.giveMeBackground(this.game.width, this.game.height));
        };
        TheCity.prototype.setSkyscraper = function () {
            var skyscraper = this.game.cache.getImage("skyscraper");
            this.skyscraper = this.game.add.tileSprite((this.game.width / 2) - (skyscraper.width / 2), this.game.height - skyscraper.height, skyscraper.width, skyscraper.height, "skyscraper");
            this.skyscraper.tint = 0x996600;
        };
        TheCity.prototype.setSkyline = function () {
            var skyline = this.game.cache.getImage("skyline");
            this.skyline = this.game.add.tileSprite(0, this.game.height - skyline.height, this.game.width, skyline.height, "skyline");
        };
        TheCity.prototype.setLights = function () {
            var lights = this.game.cache.getImage("lights");
            this.lights =
                this.game.add.tileSprite(0, this.game.height - lights.height, this.game.width, lights.height, "lights");
        };
        TheCity.prototype.giveMeBackground = function (width, height) {
            var bgBitmap = this.game.add.bitmapData(this.game.width, this.game.height);
            bgBitmap.ctx.rect(0, 0, this.game.width, this.game.height);
            bgBitmap.ctx.fillStyle = "#b2ddc8";
            bgBitmap.ctx.fill();
            return bgBitmap;
        };
        TheCity.prototype.giveMeBackgroundSprites = function () {
            var backgroundSprites = [
                { sprite: this.backgroundSprite, from: 0x1f2a27, to: 0xB2DDC8 },
                { sprite: this.skyline, from: 0x2f403b, to: 0x96CCBB },
                { sprite: this.lights, from: 0xffcc00, to: 0x00ffff }
            ];
            return backgroundSprites;
        };
        TheCity.prototype.putOnTop = function () {
            this.game.world.bringToTop(this.skyscraper);
            this.trafficServiceProvider.putOnTop();
            this.meteoServiceProvider.putOnTop();
        };
        return TheCity;
    })(Phaser.State);
    TheElevatorPitchModule.TheCity = TheCity;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var TrafficServiceProvider = (function () {
        function TrafficServiceProvider(game, utils, dayLenght) {
            this.autobus = null;
            this.CurrentDate = 0;
            this.game = game;
            this.dayLenght = dayLenght;
            this.carSprites = [];
            this.utils = utils;
        }
        TrafficServiceProvider.prototype.spawn56 = function (date, hour) {
            if (hour === 8) {
                if (this.CurrentDate === undefined) {
                    this.CurrentDate = date.getDate();
                }
                if (this.CurrentDate !== date.getDate()) {
                    this.CurrentDate = date.getDate();
                    var autobus = this.game.cache.getImage("autobus1");
                    var isRightToLeft = true;
                    var perc = 50;
                    var nextW = (autobus.width * perc) / 100;
                    var nextH = (autobus.height * perc) / 100;
                    var prevX = 0 - nextW;
                    var nextX = this.game.width;
                    var velocity = this.dayLenght;
                    var easing = Phaser.Easing.Default;
                    if (isRightToLeft) {
                        prevX = this.game.width + nextW;
                        nextX = 0 - nextW;
                        easing = Phaser.Easing.Circular.InOut;
                    }
                    this.autobus = this.game.add.sprite(prevX, this.game.height - nextH, "autobus1");
                    this.autobus.width = nextW;
                    this.autobus.height = nextH;
                    this.autobus.anchor.set(isRightToLeft ? 1 : 0, this.autobus.anchor.y);
                    var autobusTween = this.game.add.tween(this.autobus).to({ x: nextX }, velocity, easing, true)
                        .onComplete.add(function (s, t) {
                        s.destroy();
                    });
                }
            }
        };
        TrafficServiceProvider.prototype.spawnTraffic = function (options) {
            var chance = this.game.rnd.integerInRange(1, 1000) < 20;
            for (var i = 0; i < options.intervals.length; i++) {
                if (options.intervals[i].condition()) {
                    chance = this.game.rnd.integerInRange(1, 1000) < options.intervals[i].limit;
                }
            }
            if (chance) {
                var isRightToLeft = this.game.rnd.integerInRange(1, 2) === 2;
                var selector = "car" + this.game.rnd.integerInRange(1, 4);
                var car = this.game.cache.getImage(selector);
                var nextH = options.putOnTop ? 40 : 30;
                var nextW = (car.width * nextH) / car.height;
                var prevX = 0;
                var nextX = this.game.width;
                var speed = this.dayLenght;
                var easing = Phaser.Easing.Default;
                var anchorX = 0;
                var scaleX = 1;
                if (isRightToLeft) {
                    prevX = this.game.width + nextW;
                    nextX = 0 - nextW;
                    anchorX = 1;
                }
                else {
                    prevX -= nextW;
                    nextX += nextW;
                    scaleX = -1;
                }
                var carSprite = new CarBuilder(this.game, this.utils, this.dayLenght, new CarOptions(prevX, this.game.height - nextH, anchorX, nextX, nextW, nextH, scaleX, options.putOnTop, speed, easing, selector)).Create();
                if (options.putOnTop) {
                    this.carSprites.push(carSprite);
                }
                // todo: speed should be ony multiply of 10 and not based of daylight(too slow)
                // todo: easing should be random from a pool of predefined: overridable
                var carTween = this.game.add.tween(carSprite).to({ x: nextX }, speed, easing, true)
                    .onComplete.add(function (s, t) {
                    s.destroy();
                });
            }
        };
        TrafficServiceProvider.prototype.Update = function (date, hour) {
            this.spawn56(date, hour);
            var backTrafficOptions = new UpdateTrafficOptions(date, hour, false);
            backTrafficOptions.intervals.push(new TrafficIntervall(function () { return true; }, 100));
            this.spawnTraffic(backTrafficOptions);
            var trafficOptions = new UpdateTrafficOptions(date, hour, true);
            trafficOptions.intervals.push(new TrafficIntervall(function () { return (hour === 9) || (hour === 18); }, 150));
            this.spawnTraffic(trafficOptions);
        };
        TrafficServiceProvider.prototype.destroyAutobus = function (autobus) {
            autobus.destroy(true);
        };
        TrafficServiceProvider.prototype.putOnTop = function () {
            if (this.autobus !== null) {
                this.game.world.bringToTop(this.autobus);
            }
            for (var i = 0; i < this.carSprites.length; i++) {
                if (this.carSprites[i] != null) {
                    this.game.world.bringToTop(this.carSprites[i]);
                }
            }
        };
        return TrafficServiceProvider;
    })();
    TheElevatorPitchModule.TrafficServiceProvider = TrafficServiceProvider;
    var CarBuilder = (function () {
        function CarBuilder(game, utils, dayLenght, options) {
            this.game = game;
            this.dayLenght = dayLenght;
            this.options = options;
            this.utils = utils;
        }
        CarBuilder.prototype.Create = function () {
            var result = this.game.add.sprite(this.options.prevX, this.game.height - this.options.NextH, this.options.Selector);
            result.anchor.x = this.options.AnchorX;
            result.x = this.options.prevX;
            result.width = this.options.NextW;
            result.height = this.options.NextH;
            result.scale.x *= this.options.ScaleX;
            var limit = 256;
            if (!this.options.PutOnTop) {
                limit = 0;
            }
            var r = this.game.rnd.integerInRange(0, limit);
            var g = this.game.rnd.integerInRange(0, limit);
            var b = this.game.rnd.integerInRange(0, limit);
            result.tint = this.RGBtoHEX(r, g, b);
            return result;
        };
        CarBuilder.prototype.RGBtoHEX = function (r, g, b) {
            return this.utils.RGBtoHEX(r, g, b);
        };
        return CarBuilder;
    })();
    var CarOptions = (function () {
        function CarOptions(prevX, Y, AnchorX, NextX, NextW, NextH, ScaleX, PutOnTop, Speed, Easing, Selector) {
            this.prevX = prevX;
            this.Y = Y;
            this.AnchorX = AnchorX;
            this.NextX = NextX;
            this.NextW = NextW;
            this.NextH = NextH;
            this.ScaleX = ScaleX;
            this.PutOnTop = PutOnTop;
            this.Speed = Speed;
            this.Easing = Easing;
            this.Selector = Selector;
        }
        return CarOptions;
    })();
    var UpdateTrafficOptions = (function () {
        function UpdateTrafficOptions(date, hour, putOnTop) {
            this.date = date;
            this.hour = hour;
            this.putOnTop = putOnTop;
            this.intervals = [];
        }
        return UpdateTrafficOptions;
    })();
    var TrafficIntervall = (function () {
        function TrafficIntervall(condition, limit) {
            this.condition = condition;
            this.limit = limit;
        }
        return TrafficIntervall;
    })();
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
var TheElevatorPitchModule;
(function (TheElevatorPitchModule) {
    "use strict";
    var Utils = (function () {
        function Utils(game) {
            this.game = game;
        }
        Utils.prototype.tweenTint = function (sprite, startColor, endColor, duration, easing) {
            var colorBlend = { step: 0 };
            this.game.add.tween(colorBlend).to({ step: 100 }, duration, easing ? easing : Phaser.Easing.Default, false)
                .onUpdateCallback(function () {
                sprite.tint = Phaser.Color.interpolateColor(startColor, endColor, 100, colorBlend.step, 1);
            })
                .start();
        };
        Utils.prototype.RGBtoHEX = function (r, g, b) {
            return r << 16 | g << 8 | b;
        };
        return Utils;
    })();
    TheElevatorPitchModule.Utils = Utils;
})(TheElevatorPitchModule || (TheElevatorPitchModule = {}));
//# sourceMappingURL=app.js.map